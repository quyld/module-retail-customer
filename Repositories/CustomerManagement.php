<?php
/**
 * Created by mr.vjcspy@gmail.com - khoild@smartosc.com.
 * Date: 24/10/2016
 * Time: 15:22
 */

namespace SM\Customer\Repositories;


use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Setup\Exception;
use SM\Core\Api\Data\CountryRegion;
use SM\Core\Api\Data\CustomerAddress;
use SM\Core\Api\Data\CustomerGroup;
use SM\Core\Api\Data\XCustomer;
use SM\Core\Model\DataObject;
use SM\XRetail\Helper\DataConfig;
use SM\XRetail\Repositories\Contract\ServiceAbstract;
use Magento\Catalog\Model\ProductFactory;

/**
 * Class CustomerManagement
 *
 * @package SM\Customer\Repositories
 */
class CustomerManagement extends ServiceAbstract {

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $countryCollection;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    protected $customerGroupCollectionFactory;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var \Magento\Customer\Model\Config\Share
     */
    protected $configShare;
    /**
     * @var \SM\Customer\Helper\Data
     */
    protected $customerHelper;
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;
    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;
    /**
     * @var \Magento\Customer\Model\Config\Share
     */
    private $customerConfigShare;

    /**
     * @var \Magento\Wishlist\Model\WishlistFactory
     */
    protected $wishlistFactory;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productFactory;

    /**
     * @var \Magento\Wishlist\Model\ResourceModel\Item\Option\CollectionFactory
     */
    protected $wishlistItemOptionCollectionFactory;

    /**
     * @var \Magento\Wishlist\Model\ItemFactory
     */
    protected $wishlistItemFactory;
    /**
     * @var \Magento\Sales\Model\ResourceModel\Sale\CollectionFactory
     */
    protected $salesCollectionFactory;

    /**
     * CustomerManagement constructor.
     *
     * @param \Magento\Framework\App\RequestInterface                          $requestInterface
     * @param \SM\XRetail\Helper\DataConfig                                    $dataConfig
     * @param \Magento\Store\Model\StoreManagerInterface                       $storeManager
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \SM\XRetail\Helper\DataConfig $dataConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Customer\Model\Config\Share $customerConfigShare,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \SM\Customer\Helper\Data $customerHelper,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\Wishlist\Model\ResourceModel\Item\Option\CollectionFactory $wishlistItemOptionCollectionFactory,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Sales\Model\ResourceModel\Sale\CollectionFactory $salesCollectionFactory,
        ProductFactory $productFactory
    ) {
        $this->customerConfigShare                 = $customerConfigShare;
        $this->customerCollectionFactory           = $customerCollectionFactory;
        $this->countryCollection                   = $countryCollectionFactory;
        $this->resource                            = $resource;
        $this->customerGroupCollectionFactory      = $groupCollectionFactory;
        $this->customerFactory                     = $customerFactory;
        $this->customerRepository                  = $customerRepository;
        $this->customerHelper                      = $customerHelper;
        $this->addressRepository                   = $addressRepository;
        $this->addressFactory                      = $addressFactory;
        $this->wishlistFactory                     = $wishlistFactory;
        $this->productFactory                      = $productFactory;
        $this->wishlistItemFactory                 = $wishlistItemFactory;
        $this->wishlistItemOptionCollectionFactory = $wishlistItemOptionCollectionFactory;
        $this->salesCollectionFactory              = $salesCollectionFactory;
        parent::__construct($requestInterface, $dataConfig, $storeManager);
    }

    /**
     * @return array
     */
    public function getCustomerData() {
        return $this->loadCustomers($this->getSearchCriteria())->getOutput();
    }

    /**
     * @param null $searchCriteria
     *
     * @return \SM\Core\Api\SearchResult
     */
    public function loadCustomers($searchCriteria = null) {
        if (is_null($searchCriteria) || !$searchCriteria)
            $searchCriteria = $this->getSearchCriteria();

        $this->getSearchResult()->setSearchCriteria($searchCriteria);
        $collection = $this->getCustomerCollection($searchCriteria);

        $customers = [];
        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
        }
        else {
            foreach ($collection as $customerModel) {
                $customerModel->load($customerModel->getId());
                /** @var $customerModel \Magento\Customer\Model\Customer */
                $customer = new XCustomer();
                $customer->addData($customerModel->getData());
                $customer->setData('tax_class_id', $customerModel->getTaxClassId());

                $customer->setData('address', $this->getCustomerAddress($customerModel));
                $customers[] = $customer;
            }

        }

        return $this->getSearchResult()
                    ->setItems($customers)
                    ->setLastPageNumber($collection->getLastPageNumber())
                    ->setTotalCount($collection->getSize());
    }

    /**
     *
     * @param \Magento\Framework\DataObject $searchCriteria
     *
     * @return \Magento\Customer\Model\ResourceModel\Customer\Collection
     * @throws \Exception
     */
    protected function getCustomerCollection($searchCriteria) {
        $storeId = $searchCriteria->getData('storeId');
        if (is_null($storeId)) {
            throw new \Exception(__('Must have param storeId'));
        }
        else {
            $this->getStoreManager()->setCurrentStore($storeId);
        }
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $collection */
        $collection = $this->customerCollectionFactory->create();
        $collection->setCurPage(is_nan($searchCriteria->getData('currentPage')) ? 1 : $searchCriteria->getData('currentPage'));
        if ($searchCriteria->getData('ids')) {
            $collection->addFieldToFilter('entity_id', ['in' => $searchCriteria->getData('ids')]);
        }
        if ($searchCriteria->getData('entity_id') || $searchCriteria->getData('entityId')) {
            $ids = is_null($searchCriteria->getData('entity_id')) ? $searchCriteria->getData('entityId') : $searchCriteria->getData('entity_id');
            $collection->addFieldToFilter('entity_id', ['in' => explode(",", $ids)]);
        }

        if ($searchCriteria->getData('searchOnline') == 1) {
            $collection->addAttributeToSelect('*');
            $searchValue   = $searchCriteria->getData('searchValue');
            $searchField   = $searchCriteria->getData('searchFields');
            $searchPattern = [];
            foreach (explode(",", $searchField) as $field) {
                if ($field === 'first_name') {
                    $field = 'firstname';
                }
                if ($field === 'last_name') {
                    $field = 'lastname';
                }
                if ($field === 'telephone') {
                    $field = 'retail_telephone';
                }
                if ($field === 'id') {
                    $field = 'entity_id';
                }
                $searchPattern[] = ['attribute' => $field, 'like' => '%' . $searchValue . '%'];
            }
            $collection->addAttributeToFilter($searchPattern, null, 'left');
        }
        $collection->setPageSize(
            is_nan($searchCriteria->getData('pageSize')) ? DataConfig::PAGE_SIZE_LOAD_CUSTOMER : $searchCriteria->getData('pageSize')
        );
        if ($this->customerConfigShare->isWebsiteScope())
            $collection->addFieldToFilter('website_id', $this->getStoreManager()->getWebsite()->getId());

        return $collection;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getCustomerAddress(\Magento\Customer\Model\Customer $customer) {
        $customerAdd = [];

        foreach ($customer->getAddresses() as $address) {
            $addData       = $address->getData();
            $_customerAdd  = new CustomerAddress($addData);
            $customerAdd[] = $_customerAdd->getOutput();
        }

        return $customerAdd;
    }

    /**
     * @return array
     */
    public function getCountryRegionData() {
        $items      = [];
        $collection = $this->getCountryCollection($this->getSearchCriteria());
        if ($collection->getLastPageNumber() < $this->getSearchCriteria()->getData('currentPage')) {
        }
        else {
            foreach ($collection as $country) {
                /** @var \Magento\Directory\Model\Country $country */
                $regionCollection = $country->getRegionCollection();
                $regions          = [];
                foreach ($regionCollection as $region) {
                    $regions[] = $region->getData();
                }
                $countryRegion = new CountryRegion();
                $countryRegion->addData(
                    [
                        'country_id' => $country->getCountryId(),
                        'name'       => $country->getName(),
                        'regions'    => $regions
                    ]);
                $items[] = $countryRegion;
            }
        }

        return $this->getSearchResult()
                    ->setItems($items)
                    ->setTotalCount($collection->getSize())
                    ->getOutput();
    }

    /**
     * @param $searchCriteria
     *
     * @return \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected function getCountryCollection($searchCriteria) {
        /** @var   \Magento\Directory\Model\ResourceModel\Country\Collection $collection */
        $collection = $this->countryCollection->create();
        $collection->setCurPage(is_nan($searchCriteria->getData('currentPage')) ? 1 : $searchCriteria->getData('currentPage'));
        $collection->setPageSize(
            is_nan($searchCriteria->getData('pageSize')) ? DataConfig::PAGE_SIZE_LOAD_CUSTOMER : $searchCriteria->getData('pageSize')
        );

        return $collection;
    }

    /**
     * @return array
     */
    public function getCustomerGroupData() {
        $items      = [];
        $collection = $this->getCustomerGroupCollection($this->getSearchCriteria());
        if ($collection->getLastPageNumber() < $this->getSearchCriteria()->getData('currentPage')) {
        }
        else {
            foreach ($collection as $group) {
                $g = new CustomerGroup();
                /** @var \Magento\Customer\Model\Group $group */
                $g->addData(
                    [
                        'customer_group_id'   => $group->getId(),
                        'customer_group_code' => $group->getCode(),
                        'tax_class_id'        => $group->getData('tax_class_id'),
                        'tax_class_name'      => $group->getTaxClassName()
                    ]);
                $items[] = $g;
            }
        }

        return $this->getSearchResult()
                    ->setSearchCriteria($this->getSearchCriteria())
                    ->setItems($items)
                    ->setTotalCount($collection->getSize())
                    ->getOutput();
    }

    protected function getCustomerGroupCollection($searchCriteria) {
        /** @var   \Magento\Customer\Model\ResourceModel\Group\Collection $collection */
        $collection = $this->customerGroupCollectionFactory->create();
        $collection->setCurPage(is_nan($searchCriteria->getData('currentPage')) ? 1 : $searchCriteria->getData('currentPage'));
        $collection->setPageSize(
            is_nan($searchCriteria->getData('pageSize')) ? DataConfig::PAGE_SIZE_LOAD_CUSTOMER : $searchCriteria->getData('pageSize')
        );
        if ($searchCriteria->getData('entity_id')) {
            $collection->addFieldToFilter('customer_group_id', ['in' => explode(",", $searchCriteria->getData('entity_id'))]);
        }

        return $collection;
    }

    public function create($data) {
        $this->getRequest()->setParams($data);
        $customer = $this->save();
        if (isset($customer['items'][0]['id'])) {
            return $customer['items'][0]['id'];
        }
        else {
            throw new \Exception("Can't create customer");
        }
    }

    public function save() {
        $customer = $this->getRequestData();
        if (is_array($customer))
            $customer = new DataObject($customer);
        $this->customerHelper->transformCustomerData($customer);

        // This logic allows an existing customer to be added to a different store.  No new account is created.
        // The plan is to move this logic into a new method called something like 'registerAccountWithStore'
        if (!$customer->getId()) {
            try {
                $customer  = $this->customerRepository->get($customer->getEmail());
                $websiteId = $customer->getWebsiteId();

                if ($this->customerHelper->isCustomerInStore($websiteId, $customer->getStoreId())) {
                    throw new \Exception(__('This customer already exists in this store.'));
                }
            }
            catch (\Exception $e) {

            }
        }

        // Make sure we have a storeId to associate this customer with.
        if (!$customer->getStoreId()) {
            if ($customer->getWebsiteId()) {
                $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
            }
            else {
                $storeId = $this->storeManager->getStore()->getId();
            }
            $customer->setStoreId($storeId);
        }

        // Associate website_id with customer
        if (!$customer->getWebsiteId()) {
            $websiteId = $this->storeManager->getStore($customer->getStoreId())->getWebsiteId();
            $customer->setWebsiteId($websiteId);
        }

        // Update 'created_in' value with actual store name
        if ($customer->getId() === null) {
            $storeName = $this->storeManager->getStore($customer->getStoreId())->getName();
            $customer->setCreatedIn($storeName);
        }

        $customer->setAddresses(null);

        try {
            if ($customer->getId() && $customer->getId() < 1481282470403) {
                // Save lai address cu cua customer o store ben kia
                $prevCustomer = $this->getCustomerModel()->load($customer->getId());
                if ($customerId = $prevCustomer->getId()) {
                    $prevCustomer->addData($customer->getData())->save();
                }
                else {
                    throw new \Exception("Can't find customer by id");
                }
            }
            else {
                $customer->setId(null);
                if ($customer->getData("is_offline")) {
                    $prevCustomer = $this->getCustomerModel();
                    $prevCustomer->setWebsiteId($customer->getWebsiteId());
                    $prevCustomer->loadByEmail($customer->getEmail());
                    if ($prevCustomer) {
                        $prevCustomer->addData($customer->getData())->save();
                        $customerId = $prevCustomer->getId();
                    }
                    else {
                        $customer   = $this->getCustomerModel()
                                           ->addData($customer->getData())
                                           ->save();
                        $customerId = $customer->getId();
                    }
                }
                else {
                    $customer   = $this->getCustomerModel()
                                       ->addData($customer->getData())
                                       ->save();
                    $customerId = $customer->getId();
                }
            }
            $customer->setEntityId($customerId);
            $customerAdd = [];
            if (isset($customer['address'])) {
                try {
                    foreach ($customer['address'] as $address) {
                        if (!isset($address['is_save']) || $address['is_save'] !== true)
                            continue;
                        $address = new DataObject($address);
                        $this->customerHelper->transformCustomerData($address);
                        $addressModel = $this->getAddressModel();
                        if ($address->getId() && $address->getId() < 1481282470403) {
                            $addressModel->load($address->getId());
                            if (!$addressModel->getId()) {
                                throw new \Exception(__("Can't get address id: " . $address->getId()));
                            }
                        }
                        else {
                            $address->setId(null);
                        }
                        $addressModel->addData($address->getData());
                        $addressModel->setData('parent_id', $customerId)
                                     ->save();
                        $_customerAdd  = new CustomerAddress($addressModel->getData());
                        $customerAdd[] = $_customerAdd->getOutput();
                    }
                }
                catch (InputException $e) {
                    throw $e;
                }
            }
        }
        catch (AlreadyExistsException $e) {
            throw new \Exception(
                __('A customer with the same email already exists in an associated website.')
            );
        }
        catch (LocalizedException $e) {
            throw $e;
        }
        $data = new XCustomer();
        $data->addData($customer->getData());
        $data->setData('address', $customerAdd);

        return $this->getSearchResult()
                    ->setItems([$data])
                    ->setLastPageNumber(1)
                    ->setTotalCount(1)
                    ->getOutput();
    }

    public function createAddress() {
        $address = $this->getRequestData();
        $this->customerHelper->transformCustomerData($address);

        $addressModel = $this->getAddressModel();
        if ($address->getId()) {
            $addressModel->load($address->getId());
            if (!$addressModel->getId()) {
                throw new \Exception(__("Can't get address id: " . $address->getId()));
            }
        }
        $addressModel->addData($address->getData());
        $addressModel->setData('parent_id', $address->getData('customer_id'));

        $addData      = $addressModel->save()->getData();
        $_customerAdd = new CustomerAddress($addData);

        return $_customerAdd->getOutput();
    }

    /**
     * @return \Magento\Customer\Model\Address
     */
    protected function getAddressModel() {
        return $this->addressFactory->create();
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    protected function getCustomerModel() {
        return $customerModel = $this->customerFactory->create();
    }

    private function dummy($isDummyAdd = false) {
        /*
         * Create customer: Post: mage212.dev/xrest/v1/xretail/customer
         * Edit customer: Put: mage212.dev/xrest/v1/xretail/customer
         * Add Customer address: Post: mage212.dev/xrest/v1/xretail/customer-address
         * Edit Customer address: Put: mage212.dev/xrest/v1/xretail/customer-address
         * */
        if (!$isDummyAdd) {
            $data = [
                "id"          => 22,
                "group_id"    => 2,
                "email"       => "mr.vjcspy" . rand(0, 100000) . "@gmail.com",
                "first_name"  => "Mr" . rand(0, 100000),
                "last_name"   => "Abc" . rand(0, 100000),
                "middle_name" => "xyz" . rand(0, 100000),
                "prefix"      => "a",
                "suffix"      => "b",
                "gender"      => 0,
                "storeId"     => 1,
            ];
        }
        else {
            $data = [
                "id"          => 7, // Nếu add thì id =0, edit thì có id
                "customer_id" => 22,
                "region_id"   => 0,
                "country_id"  => "string",
                "street"      => [
                    "string"
                ],
                "telephone"   => "string",
                "postcode"    => "string",
                "city"        => "string",
                "first_name"  => "string",
                "last_name"   => "string",
                "middle_name" => "string",
            ];
        }
        $this->getRequest()->setParams($data);
    }

    public function loadCustomerDetail($searchCriteria = null) {
        if (is_null($searchCriteria) || !$searchCriteria) {
            $searchCriteria = $this->getSearchCriteria();
        }
        $this->getSearchResult()->setSearchCriteria($searchCriteria);
        $customerId = $searchCriteria->getData('customerId');
        $storeId    = $searchCriteria->getData('storeId');
        if (is_null($customerId) || is_null($storeId)) {
            throw new Exception(__("Something wrong! Missing require value"));
        }
        $data     = [];
        $wishlist = $this->getWishlistData($customerId, $storeId);

        $customerLifetimeSales   = $this->salesCollectionFactory->create()->setOrderStateFilter(Order::STATE_COMPLETE, true)
                                                                ->setCustomerIdFilter($customerId)->load()->getTotals()->getLifetime();
        $data['life_time_sales'] = $customerLifetimeSales;
        $data['wishlist']        = $wishlist;

        return $data;
    }

    protected function getWishlistData($customerId, $storeId) {
        $wishlistFactory = $this->wishlistFactory->create();
        $wishlistItems   = $wishlistFactory->loadByCustomerId($customerId, true)->getItemCollection()->getData();
        if (sizeof($wishlistItems) == 0) {
            return;
        }
        else {
            $items = [];
            foreach ($wishlistItems as $item) {
                $productId = $item['product_id'];
                $store     = $item['store_id'];
                if ($storeId == $store && !is_null($store) && !is_null($productId)) {
                    $options = $this->wishlistItemOptionCollectionFactory->create()->addFieldToFilter('code', "info_buyRequest")->addItemFilter(
                        $item);;
                    $listOption = unserialize($options->getFirstItem()->getValue());
                    if (isset($listOption['options']) && is_array($listOption['options'])) {
                        foreach ($listOption['options'] as &$option) {
                            if (is_array($option))
                                if ($option['date_internal']) {
                                    unset($option['date_internal']);
                                }
                        }
                    }
                    if (isset($listOption['custom_price'])) {
                        unset($listOption['custom_price']);
                    }

                    $itemValue = $listOption;
                    //$item['price'] = $productsCollection->getPrice();
                    $itemValue['product_id']       = $productId;
                    $itemValue['wishlist_item_id'] = $item['wishlist_item_id'];
                    $itemValue['isChecked']        = false;
                    $items[]                       = $itemValue;
                }
            }
        }

        return $items;
    }

    public function deleteItemWishlist() {
        $customerId   = $this->getRequest()->getParam('customer_id');
        $wishlistItem = $this->getRequest()->getParam('items');
        if (is_null($customerId) || !is_array($wishlistItem) || count($wishlistItem) < 1) {
            throw new Exception(__('Something wrong! Missing require value'));
        }
        foreach ($wishlistItem as $wishlist) {
            $productId = $wishlist['wishlist_item_id'];

            $itemCollection = $this->wishlistItemFactory->create()->load($productId);
            if (!$itemCollection->getId()) {
                throw new Exception(__("Can not find item in wish list"));
            }
            else {
                $itemCollection->delete();
            }
        }

        return ["success" => true, "message" => __('Selected item(s) are added to cart')];
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductModel() {
        return $this->productFactory->create();
    }
}